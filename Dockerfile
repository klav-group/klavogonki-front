# Use the official lightweight Node.js 16 image.
# https://hub.docker.com/_/node
FROM node:16-slim

# Copy application dependency manifests to the container image.
# A wildcard is used to ensure both package.json AND package-lock.json are copied.
# Copying this separately prevents re-running npm install on every code change.
COPY package*.json /usr/src/app/

# Install production dependencies.
RUN npm --prefix /usr/src/app/ install

# Copy local code to the container image.
COPY . /usr/src/app/
WORKDIR /usr/src/app

# Run the web service on container startup.
ENTRYPOINT [ "npm", "start" ]
