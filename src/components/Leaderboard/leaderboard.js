import React, { useContext, useEffect } from 'react';
import './leaderboard.css';
import { observer } from 'mobx-react-lite';
import Context from '../../context';

import { Link } from 'react-router-dom';

function Leaderboard() {
  const { leaderboardStore } = useContext(Context);


  useEffect(() => {
    if (!leaderboardStore.filled) {
      leaderboardStore.getTop()
    }
  });

  return (
    <div className="App">

      <Link to="/">
        <button class="btn btn-light">
          <span>Home</span>
        </button>
      </Link>

      <Link to="/profile">
        <button style={{ marginLeft: 10 }} class="btn btn-light">
          <span>Profile</span>
        </button>
      </Link>

      <table style={{ marginTop: 15 }}>
        <thead>
          <tr>
            <th>Date</th>
            <th>User</th>
            <th>WPM</th>
          </tr>
        </thead>
        <tbody>
          {leaderboardStore.leaderboard.map(item => {
            if (!isNaN(item.speed) && item.speed != Infinity && item.speed != -Infinity) {
              return (
                <tr>
                  <td>{item.timestamp}</td>
                  <td>{item.username}</td>
                  <td>{item.speed.toFixed(2)}</td>
                </tr>
              );
            }
          })}
        </tbody>
      </table>
    </div>
  );
}

export default observer(Leaderboard)
