import $api from '../http';

export default class LeaderboardService {
    static async getTop(maxRows=999) {
        return $api.get('/game/stats/getTop?maxRows=' + maxRows)
    }

}
