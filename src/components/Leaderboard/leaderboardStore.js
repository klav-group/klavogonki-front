import {makeAutoObservable} from 'mobx';
import LeaderboardService from './LeaderboardService';

export default class LeaderboardStore {
    leaderboard = [];
    filled = false;

    constructor() {
        makeAutoObservable(this)
    }

    setFilled(bool) {
        this.filled = bool;
    }

    setLeaderboard(leaderboard) {
        this.leaderboard = leaderboard;
    }

    async getTop() {
        try {
            const response = await LeaderboardService.getTop()
            this.setFilled(true)
            this.setLeaderboard(response.data.sort((a, b) => parseFloat(b.speed) - parseFloat(a.speed)))
        } catch (e) {
            this.setFilled(false)
            console.log(e.response?.data?.message)
        }
    }
}
