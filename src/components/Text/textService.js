import $api from "../http";
import { API_URL } from "../http";

export function Generate(count=20, lang="russian") {
    return $api.get(API_URL + '/game/text?count=' + count + '&lang=' + lang);
}

export function GetLangs() {
    return ["english", "russian", "java", "kotlin", "python"];
    // return $api.get(API_URL + "/game/langs");    
}