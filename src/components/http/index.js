import axios from 'axios';
//export const API_URL = 'http://localhost:8080';
export const API_URL = 'https://klavagonki-back-hkr67jx3ta-lm.a.run.app';

const $api = axios.create({
  withCredentials: true,
  baseURL: API_URL,
});

$api.interceptors.request.use((config) => {
  config.headers.Authorization = `${localStorage.getItem('token')}`;
  return config;
});

$api.interceptors.response.use((config) => {
  return config;
}, async (error) => {
  const originalRequest = error.config;
  if ((error.response.status === 401 || error.response.status === 403) && error.config && !error.config._isRetry) {
    console.log('retry');
    originalRequest._isRetry = true;
    const response = await axios.get(`${API_URL}/refresh_token`, {withCredentials: true});
    localStorage.setItem('token', response.data.token);
    localStorage.setItem('username', response.data.username);
    return $api.request(originalRequest);
  }
  throw error;
});

export default $api;
