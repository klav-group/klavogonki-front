import $api from "../http";
import { API_URL } from "../http";

export async function GetSaved(username) {
    try {
        const response = await $api.get('game/allusertexts?username=' + username)
        return response.data

    } catch (e) {
        console.error(e)
    }
}

export async function SaveText(username, textname, text) {
    try {
        console.log("ssaaave")

        await $api.put('game/usertext', {
            username: username,
            name: textname,
            text: text
        })
        
    } catch (e) {
        console.error(e)
    }
}