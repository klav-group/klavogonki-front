import React, {useContext, useEffect} from 'react';
import './profile.css';
import Context from '../../context';
import {observer} from 'mobx-react-lite';

import {CartesianGrid, Legend, Line, LineChart, Tooltip, XAxis, YAxis} from 'recharts';
import {Link} from 'react-router-dom';

function Profile() {
  const {
    store,
    profileStore
  } = useContext(Context);

  useEffect(() => {
    if (!profileStore.filled) {
      profileStore.getStats(store.username);
    }
  });

  return (
    <div className="App">

      <Link to="/">
        <button class="btn btn-light">
          <span>Home</span>
        </button>
      </Link>

      <Link to="/leaderboard">
        <button style={{marginLeft: 10}} class="btn btn-light">
          <span>Leaderboard</span>
        </button>
      </Link>

      <button style={{marginLeft: 10}} className="btn btn-light" onClick={() => {
          store.logout();
          window.location.reload();
        }
      }>
        <span>Logout</span>
      </button>


      <h3 style={{marginTop: 15}}>
        {store.isAuth ? `User: ${store.username}` : 'Not auth'}
      </h3>

      <LineChart width={700} height={400} data={profileStore.stats} margin={{
        top: 5,
        right: 20,
        bottom: 5,
        left: 0
      }} className="mb-3 mt-3">
        <Line type="monotone" dataKey="speed" stroke="#8884d8" />
        <CartesianGrid stroke="#ccc" strokeDasharray="5 5" />
        <XAxis dataKey="timestamp" tick="" />
        <YAxis dataKey="speed" label={{
          value: 'WPM',
          angle: -90,
          position: 'insideLeft',
          offset: 10
        }} />
        <Legend />
        <Tooltip />
      </LineChart>

      <table>
        <thead>
        <tr>
          <th>Date</th>
          <th>WPM</th>
        </tr>
        </thead>
        <tbody>
        {profileStore.stats.map(item => {
          return (
            <tr>
              <td>{item.timestamp}</td>
              <td>{item.speed.toFixed(2)}</td>
            </tr>
          );
        })}
        </tbody>
      </table>
    </div>

  );
}

export default observer(Profile);
