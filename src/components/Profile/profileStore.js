import {makeAutoObservable} from 'mobx';
import $api from '../http';

export default class ProfileStore {
    stats = [];
    filled = false;

    constructor() {
        makeAutoObservable(this)
    }

    setStats(stats) {
        this.stats = stats;
    }

    setFilled(bool) {
        this.filled = bool;
    }

    async getStats(username) {
        try {
            const response = await $api.get('/game/stats/getAllByUser?username=' + username);
            // this.setStats(response.data)

            // var data = [
            //     {"speed":60.51,"timestamp":"2021-11-02 06:38:51"},
            //     {"speed":30.51,"timestamp":"2021-11-03 06:38:51"},
            //     {"speed":25.51,"timestamp":"2021-11-10 08:38:51"},
            //     {"speed":36.51,"timestamp":"2021-11-14 08:38:51"},
            //     {"speed":70.51,"timestamp":"2021-11-24 08:38:51"},
            //     {"speed":40.51,"timestamp":"2021-11-04 06:38:51"},
            //     {"speed":55.51,"timestamp":"2021-11-04 08:38:51"},
            //     {"speed":100.51,"timestamp":"2021-11-06 08:38:51"},
            // ]

            var data = response.data.sort(function(x, y) {
                return Date.parse(x.timestamp) - Date.parse(y.timestamp);
            });

            this.setStats(data)

            this.filled = true;
        } catch (e) {
            console.log(e.response?.data?.message)
        }
    }

}
