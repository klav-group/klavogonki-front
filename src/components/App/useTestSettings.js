import { useState } from 'react';

export default function useTestSettings() {

    const getWordCount = () => {
        const wordCountString = localStorage.getItem('wordCount');
        const wordCount = JSON.parse(wordCountString);
        
        if (wordCount == null) {
            return "20";
        }

        return wordCount
    };

    const [wordCount, setWordCount] = useState(getWordCount());

    const saveWordCount = (newWordCount, save=true) => {
        if (save == true) {
            localStorage.setItem('wordCount', JSON.stringify(newWordCount));
            console.log("saved");
        }
        setWordCount(newWordCount)
    };

    const resetWordCount = () => {
        setWordCount(getWordCount());
    }

    const [lang, setLang] = useState("russian");

    return {
        setTotalWordCount: saveWordCount,
        totalWordCount: wordCount,
        getStoredWordCount: getWordCount,
        lang: lang,
        setLang, setLang
    }
}
