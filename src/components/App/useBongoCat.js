import { useState } from 'react';

export default function useBongoCat() {

    const maxBongoCatId = 12;
    const [curBongoCatId, setCurBongoCatId] = useState(0);
    let bongoCat = `./bongo_cats/frame_${curBongoCatId.toString()}.gif`;

    const changeBongoCat = () => {
        setCurBongoCatId((curBongoCatId + 1) % maxBongoCatId);
    }

    return {
        bongoCat,
        changeBongoCat
    }
}