import $api from "../http";
import { API_URL } from "../http";
import { currentTime } from '../../utils/time';

export default function SendResult(username, wpm) {
    return $api.put(API_URL + '/game/stats/sendResult', {
        username,
        speed: wpm,
        timestamp: currentTime()
    });
}