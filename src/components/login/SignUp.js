import React, {useContext, useState} from 'react';
import Context from '../../context';
import {observer} from 'mobx-react-lite';
import {Link, Redirect} from 'react-router-dom';

function SignUp() {
    const [username, setUserName] = useState();
    const [password, setPassword] = useState();
    const [email, setEmail] = useState();

    const {store} = useContext(Context);

    return (
        <div className="auth-wrapper">
            <div className="auth-inner">
                <form onSubmit={(event) => {
                    event.preventDefault();
                    store.registration(username, email, password);
                }
                }>
                    { store.succReg ? (<Redirect push to="/profile"/>) : null }

                    <h3>Sign Up</h3>

                    <div className="form-group">
                        <label>Username</label>
                        <input type="text" className="form-control" placeholder="Enter username" onChange={e => setUserName(e.target.value)} />
                    </div>

                    <div className="form-group">
                        <label>Email address</label>
                        <input type="email" className="form-control" placeholder="Enter email" onChange={e => setEmail(e.target.value)} />
                    </div>

                    <div className="form-group">
                        <label>Password</label>
                        <input type="password" className="form-control" placeholder="Enter password" onChange={e => setPassword(e.target.value)} />
                    </div>

                    <button type="submit" className="btn btn-primary btn-block mt-3">Sign Up</button>
                    <p className="forgot-password text-right">
                        Already registered <Link to={'/profile'}>sign in?</Link>
                    </p>
                </form>
            </div>
        </div>
    );
}

export default observer(SignUp)
