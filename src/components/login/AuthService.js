import $api from '../http';

export default class AuthService {
    static async login(username, password) {
        return $api.post('/account/login', {username, password})
    }

    static async registration(username, email,  password) {
        return $api.post('/account', {username, email, password})
    }

    static async logout() {
        //return $api.post('/account/logout') todo
    }
 }
