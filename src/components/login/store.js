import AuthService from './AuthService';
import {makeAutoObservable} from 'mobx';
import axios from 'axios';
import {API_URL} from '../http';

export default class Store {
  username = '';
  isAuth = false;
  succReg = false;

  constructor() {
    makeAutoObservable(this);
  }

  setAuth(bool) {
    this.isAuth = bool;
  }

  setUsername(username) {
    this.username = username;
  }

  setSuccReg(reg) {
    this.succReg = reg;
  }

  async login(username, password) {
    try {
      const response = await AuthService.login(username, password);
      console.log(response);
      localStorage.setItem('token', response.data.token);
      localStorage.setItem('username', username);
      this.setAuth(true);
      this.setUsername(username);
    } catch (e) {
      if (e.response.status === 403) {
        alert('Login and password incorrect');
      }
      console.log(e.response?.data?.message);
    }
  }

  async registration(username, email, password) {
    try {
      const response = await AuthService.registration(username, email, password);
      if (response.status === 200) {
        this.setSuccReg(true);
      }
    } catch (e) {
      if (e.response.status === 409) {
        alert('Account with the same login already created');
      }
      console.log(e.response?.data?.message);
    }
  }

  async logout() {
    try {
      const response = await AuthService.logout();
      console.log(response);
      localStorage.removeItem('token');
      localStorage.removeItem('username');
      this.setAuth(false)
    } catch (e) {
      console.log(e.response?.data?.message);
    }
  }

  async checkAuth() {
    try {
      const response = await axios.get(`${API_URL}/refresh_token`, {withCredentials: true});
      localStorage.setItem('token', response.data.token);
      this.setAuth(true);
      this.setUsername(response.data.username);
    } catch (e) {
      this.setAuth(false);
      console.log(e.response?.data?.message);
    }
  }
}
