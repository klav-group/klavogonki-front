import React, {useContext, useState} from 'react';
import Context from '../../context';
import {Link, Redirect} from 'react-router-dom';
import {observer} from 'mobx-react-lite';


function Login() {
  const [username, setUserName] = useState('');
  const [password, setPassword] = useState('');
  const {store} = useContext(Context);

  return (
    <div className="auth-wrapper">
      <div className="auth-inner">
        <form onSubmit={(event) => {
            event.preventDefault()
            store.login(username, password)
            return <Redirect to={'/profile'}/>;
        }}>
            <h3>Sign In</h3>

          <div className="form-group">
            <label>Username</label>
            <input
                type="text"
                className="form-control"
                placeholder="Enter username"
                onChange={e => setUserName(e.target.value)}
                value={username}
            />
          </div>
          <div className="form-group">
            <label>Password</label>
            <input
                type="password"
                className="form-control"
                placeholder="Enter password"
                onChange={e => setPassword(e.target.value)}
                value={password}
            />
          </div>

          <button type={'submit'} className="btn btn-primary btn-block mt-3">Submit</button>
        </form>
          <br/>
          <Link to="/signup">
              <button>
                  <span>Registration</span>
              </button>
          </Link>
      </div>
    </div>
  );
}

export default observer(Login)
