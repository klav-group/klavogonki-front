import React, {createContext} from 'react';

import Store from './components/login/store';
import ProfileStore from './components/Profile/profileStore';
import LeaderboardStore from './components/Leaderboard/leaderboardStore';

export const store = new Store();
export const profileStore = new ProfileStore();
export const leaderboardStore = new LeaderboardStore()

export const Context = createContext({
    store,
    profileStore,
    leaderboardStore,
});

export default Context;