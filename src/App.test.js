import { render, screen } from '@testing-library/react';
import App from './App';

test('custom text checkbox should be hidden by default', () => {
  const result = render(<App />);
  const elem = result.container.querySelector("#customTextCheckbox");
  expect(elem).not.toBeInTheDocument();
});

test('profile button must be present on the start page', () => {
  const result = render(<App />);
  const elem = result.container.querySelector("#profileButton");
  expect(elem).toBeInTheDocument();
});

test('leaderboard button must be present on the start page', () => {
  const result = render(<App />);
  const elem = result.container.querySelector("#leaderboardButton");
  expect(elem).toBeInTheDocument();
});