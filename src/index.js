import React, {createContext} from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import Context from "./context"
import { store, profileStore, leaderboardStore } from "./context";

ReactDOM.render(
    <Context.Provider value={{
        store,
        profileStore,
        leaderboardStore
    }}>
        <App />
    </Context.Provider>,
    document.getElementById('root')
);
