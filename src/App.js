import './App.css';
import React, { useContext, useEffect, useState } from 'react';

import useKeyPress from './hooks/useKeyPress';
import './styles.css';

import { currentTime } from './utils/time';
import { BrowserRouter, Link, Route, Switch } from 'react-router-dom';

import Profile from './components/Profile/profile';
import Login from './components/login/Login';
import SignUp from './components/login/SignUp';
import Leaderboard from './components/Leaderboard/leaderboard';
import { Generate, GetLangs } from './components/Text/textService';
import { GetSaved, SaveText } from './components/Custom/customService';
import SendResult from './components/Result/resultService'

import useBongoCat from './components/App/useBongoCat';
import useTestSettings from './components/App/useTestSettings';

import Loader from 'react-loader-spinner';
import '../node_modules/bootstrap/dist/css/bootstrap.min.css';
import Context from './context';
import { observer } from 'mobx-react-lite';

var initialWords = Generate();
let langs = GetLangs();

function App() {

    const { store } = useContext(Context)
    useEffect(() => {
        if (localStorage.getItem('token')) {
            store.setAuth(true)
            store.setUsername(localStorage.getItem('username'))
        }
    });

    const { bongoCat, changeBongoCat } = useBongoCat();
    const { totalWordCount, setTotalWordCount, getStoredWordCount, lang, setLang } = useTestSettings();

    const [leftPadding, setLeftPadding] = useState(
        new Array(20).fill(' ').join(''),
    );

    const [outgoingChars, setOutgoingChars] = useState('');
    const [currentChar, setCurrentChar] = useState('');
    const [incomingChars, setIncomingChars] = useState('');

    const [testDone, setTestDone] = useState(false);

    const [showLoader, setShowLoader] = useState("block");
    const [showText, setShowText] = useState("none");

    const [isCustom, setIsCustom] = useState(true);
    const [savedTexts, setSavedTexts] = useState([]);

    const [reload, setReload] = useState(1);

    const reset = (text) => {
        console.log(text)
        setCurrentChar(text.charAt(0));
        setIncomingChars(text.substr(1));
        setOutgoingChars('');
        setStartTime();
        setWordCount(0);
        setWpm(0);
        setAccuracy(0);
        setTypedChars('');
        setTestDone(false);
        setShowLoader("none");
        setShowText("block");
        setIsCustom(false);
    }

    const processInitialWords = () => {
        setShowText('none');
        setShowLoader('block');

        initialWords.then(response => {
            if (response.status === 200) {
                reset(response.data.text);
            }
        }).catch((error) => {
            console.error(error);
        });
    };
    useEffect(() => { processInitialWords() }, [reload]);

    const [startTime, setStartTime] = useState();
    const [wordCount, setWordCount] = useState(0);
    const [wpm, setWpm] = useState(0);

    const [accuracy, setAccuracy] = useState(0);
    const [typedChars, setTypedChars] = useState('');


    useKeyPress(key => {

        if (!startTime) {
            setStartTime(currentTime());
        }

        let updatedOutgoingChars = outgoingChars;
        let updatedIncomingChars = incomingChars;

        if (key === "Escape") {
            let newCnt = getStoredWordCount()
            initialWords = Generate(newCnt, lang);
            setTotalWordCount(newCnt);
            setReload(reload + 1);
        }

        if (key === currentChar) {

            if (leftPadding.length > 0) {
                setLeftPadding(leftPadding.substring(1));
            }

            updatedOutgoingChars += currentChar;
            setOutgoingChars(updatedOutgoingChars);

            setCurrentChar(incomingChars.charAt(0));

            updatedIncomingChars = incomingChars.substring(1);

            setIncomingChars(updatedIncomingChars);

            if (incomingChars.charAt(0) === ' ' || incomingChars.charAt(0) === '') {
                setWordCount(wordCount + 1);
                const durationInMinutes = (currentTime() - startTime) / 60000.0;
                setWpm(((wordCount + 1) / durationInMinutes).toFixed(2));
            }
        }

        if (!store.isAuth) {
            return (
                <Login />
            )
        }

        if (updatedIncomingChars !== '') {
            const updatedTypedChars = typedChars + key;
            setTypedChars(updatedTypedChars);
            setAccuracy(
                ((updatedOutgoingChars.length * 100) / updatedTypedChars.length).toFixed(
                    2,
                ),
            );
        }

        changeBongoCat();

        if (wordCount == totalWordCount) {
            console.log("norm")
            console.log(!testDone)
            console.log(store.username)
            console.log(store.isAuth)
            if (!testDone && store.username && store.isAuth) {
                SendResult(store.username, wpm)
                alert("Test finished successfully!")
            }
            setTestDone(true)
        }
    });

    const handleSelectWordCount = (e) => {
        setTotalWordCount(e.target.value)
        initialWords = Generate(e.target.value, lang);
        setReload(reload + 1);
    }

    const handleSelectLanguage = (e) => {
        console.log(e.detail)

        if (e.target.value == "other") {
            const enteredText = prompt('Please enter your text');

            if (enteredText != null && enteredText.replace(/\s/g, '').length) {
                setTotalWordCount(enteredText.split(" ").length, false)
                reset(enteredText, false);
            }

        } else {
            UseLang(e.target.value)
        }
    }

    async function handleAddTextButton() {
        const enteredText = prompt('Please enter your text');
        const textName = prompt('Please enter text name');

        if (!validateText(enteredText)) {
            alert("Please enter valid text");
            return;
        }

        if (!(await validateName(textName))) {
            alert("Please enter valid name");
            return;
        }

        await SaveText(store.username, textName, enteredText)
        setSavedTexts(await GetSaved(store.username))
    }

    const validateText = (text) => {
        return text != null && text.replace(/\s/g, '').length;
    }

    async function validateName(name) {
        const texts = await GetSaved(store.username);
        let flag = true;

        texts.forEach(elem => {
            if (elem.name === name || !validateText(elem.name)) {
                flag = false;
            }
        });

        return flag;
    }

    const handleUseSavedTextButton = (text) => {
        setTotalWordCount(text.split(" ").length, false)
        reset(text, false);
    }

    function UseLang(lang = "english") {
        setLang(lang)
        initialWords = Generate(totalWordCount, lang);
        setReload(reload + 1);
    }

    async function handleCustomLangCheck(e) { 
        setIsCustom(!isCustom)
        const value = e.target.checked
        
        if (value) {
            setSavedTexts(await GetSaved(store.username))
        }
    }

    function CustomTextCard({name, text}) {
        return (
            <div className='mb-2' id="card">
                <br></br>
                <h2>{name}</h2>
                <p class="cut-text">{text}</p>
                <span class="left bottom">Word count: {text.split(" ").length}</span>
                <button class="right bottom btn btn-light" onClick={() => handleUseSavedTextButton(text)}>Use</button>
            </div>
        )
    }

    return (
        <div className="App">
            <BrowserRouter>
                <Switch>
                    <Route path="/leaderboard">
                        <Leaderboard />
                    </Route>

                    <Route path="/profile">
                        {store.isAuth ? (
                            <Profile />
                        ) : (
                            <Login />
                        )}
                    </Route>

                    <Route path="/signup">
                        <SignUp />
                    </Route>

                    <Route path="/">
                        <header>
                            <Link to="/leaderboard">
                                <button id="leaderboardButton" class="btn btn-light">
                                    <span>Leaderboard</span>
                                </button>
                            </Link>

                            <Link to="/profile">
                                <button id="profileButton" style={{ marginLeft: 10 }} class="btn btn-light">
                                    <span>Profile</span>
                                </button>
                            </Link>
                        </header>

                        <header className="App-header">
                            <h5 className="Character">
                                {wordCount} / {totalWordCount}
                            </h5>
                            <Loader
                                type="ThreeDots"
                                color="#00BFFF"
                                height={70}
                                width={70}
                                style={{ display: showLoader }}
                            />
                            <p className="Character" style={{ display: showText }}>
                                <span className="Character-out">
                                    {(leftPadding + outgoingChars).slice(-35)}
                                </span>
                                <span className="Character-current">{currentChar}</span>
                                <span>{incomingChars.substr(0, 35)}</span>
                            </p>
                            <h5 className="Character">
                                WPM: {wpm} | ACC: {accuracy}%
                            </h5>

                            <img src={bongoCat} className="bongocat" alt="bongocat" />
                            <h6 className="Character mb-5">
                                esc - reload text
                            </h6>

                            <div style={{ display: "flex", "flex-direction": "row", "justify-content": "center", "align-items": "center" }}>

                                {!isCustom && (
                                    <div style={{ marginRight: 20 }}>
                                        <h6 className="Character">Word count:</h6>
                                        <select name="wordCountSelector"
                                            id="wordCountSelector"
                                            value={totalWordCount}
                                            onChange={handleSelectWordCount}>
                                            <option>20</option>
                                            <option>40</option>
                                            <option>80</option>
                                            <option>160</option>
                                        </select>
                                    </div>
                                )}

                                {!isCustom && (
                                    <div style={{ marginRight: 20 }}>
                                        <h6 className="Character">Language:</h6>
                                        <select name="langSelector"
                                            value={lang}
                                            onChange={handleSelectLanguage}>
                                            <LangOptions></LangOptions>
                                            {!store.isAuth && (<option>other</option>)}
                                        </select>
                                    </div>
                                )}

                                {store.isAuth && (
                                    <div>
                                        <h6 className="Character">Custom:</h6>
                                        <input id="customTextCheckbox" type="checkbox" checked={isCustom} onChange={handleCustomLangCheck}/>
                                    </div>
                                )}

                                {isCustom && (
                                    <button class="btn btn-outline-secondary" style={{ marginLeft: 20 }} onClick={handleAddTextButton}>Add text</button>
                                )}

                            </div>

                            {isCustom && savedTexts &&
                                savedTexts.map(element => (
                                    <CustomTextCard name={element.name} text={element.text}></CustomTextCard>
                                ))
                            }

                        </header>
                    </Route>
                </Switch>
            </BrowserRouter>
        </div>
    );
}

function LangOptions() {
    return (langs.map(element =>
        <option>{element}</option>)
    );
}

export default observer(App);
